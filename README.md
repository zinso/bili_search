# Command line tools for search bilibili in ternimal
[![Crates.io](https://img.shields.io/crates/v/bili_search.svg)](https://crates.io/crates/bili_search)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://framagit.org/zinso/bili_search)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://framagit.org/zinso/bili_search/-/raw/master/LICENSE)
![bump.gif](https://framagit.org/zinso/bili_search/-/raw/master/bili.gif)
## Install:
```sh
cargo install bili_search
```
## Example:
```
$ bili_search rust crash course
Rust Course 2021 by the book | RustLang
https://www.bilibili.com/video/BV1WA4y1o7yP
终极Rust速成课程 Ultimate Rust Crash Course
https://www.bilibili.com/video/BV1VS4y1G75u
Rust Crash Course
https://www.bilibili.com/video/BV1xb411v7Mh
Rust Crash Course | Rustlang
https://www.bilibili.com/video/BV1PZ4y1H736
Rust Crash Course  Rustlang
https://www.bilibili.com/video/BV1S3411g7dp
Rust Crash Course _ Rustlang rust 基础语法教程
https://www.bilibili.com/video/BV1b741167ga
[Traversy Media] - 优秀编程语言 Rust 2 小时入门 - Rust Crash Course | Rustlang
https://www.bilibili.com/video/BV16K411K7s8
Rust 教程(Rust Programming Course for Beginners - Tutorial)
https://www.bilibili.com/video/BV12V4y1G7NY
Rust Web 全栈开发教程【完结】
https://www.bilibili.com/video/BV1RP4y1G7KF
【中英文字幕】Rust - 4小时完整入门课程
https://www.bilibili.com/video/BV1Br4y1b7CU
Rust编程语言入门教程（Rust语言/Rust权威指南配套）【已完结】
https://www.bilibili.com/video/BV1hp4y1k7SV
【Rust编程入门教程】Rust系列教程，国外大牛讲解
https://www.bilibili.com/video/BV1VJ411A7Lc
Rust Tutorial Full Course
https://www.bilibili.com/video/BV1yW4y187kK
半小时弄懂-Rust基本语法及语言特性
https://www.bilibili.com/video/BV15U4y177oh
Rust编程视频教程（进阶）
https://www.bilibili.com/video/BV1FJ411Y71o
1️⃣0️⃣0️⃣秒了解Rust
https://www.bilibili.com/video/BV19N4y1K7rw
【YouTube热门+中文字幕】Python开发者学习Rust最佳路径 - From Python to Rust
https://www.bilibili.com/video/BV1yG4y1n769
用Rust进行系统编程的实践  Hands-On Systems Programming with Rust
https://www.bilibili.com/video/BV1Aa411y7Gh
随便聊聊 Rust - 面向新手的 Rust 入门指南
https://www.bilibili.com/video/BV1984y1y7yF
Rust Async 异步编程 简易教程
https://www.bilibili.com/video/BV16r4y187P4
【2022】最新最全Rust编程语言入门教程
https://www.bilibili.com/video/BV16B4y1q7Sq
Rust编程零基础快速入门指南，挑战双高语言
https://www.bilibili.com/video/BV19g411g7qi
Rust 编程语言中级教程（更新ing）
https://www.bilibili.com/video/BV1TF411P74m
Rust 编程：完整的开发者指南(上) Rust Programming: The Complete Developer;#39;s Guide
https://www.bilibili.com/video/BV1tm4y1Z7HX
Rust语言：完整的初学者指南  Rust lang: The complete beginner;#39;s guide
https://www.bilibili.com/video/BV1a44y1s7mA
【RustLang】两小时零基础入门学会rust语言！！！连续4年最受欢迎的语言！！！
https://www.bilibili.com/video/BV1M4411H7tX
Rust Async 异步编程（更新中）
https://www.bilibili.com/video/BV1hS4y1W74D
【中文字幕】Rust 让你感觉自己是个天才
https://www.bilibili.com/video/BV11g411y7JE
Rust 代码挑战 Rust Code Challenges
https://www.bilibili.com/video/BV1DT411u7ia
【Rust】SolidJS in Rust with Sycamore！
https://www.bilibili.com/video/BV14m4y1c7YV
Rust入门教程
https://www.bilibili.com/video/BV1mt41197vx
2-2-为什么RUST可执行文件如此庞大，如何减小RUST可执行文件大小《跟星哥一起学Rust语言》
https://www.bilibili.com/video/BV11L4y157Zr
Rust IDE选择和设置
https://www.bilibili.com/video/BV1aP41137cm
【Rust 唠嗑室】第42期：字节开源的高性能 Rust RPC 框架：Volo 的那些事
https://www.bilibili.com/video/BV1FG4y1h79w
Rust 过程宏（第一弹）
https://www.bilibili.com/video/BV1Za411q7LQ
RUST世界赛！！世界上所有最好的rust主播参与！！比赛解说: Hjune等。Posty视角！！！
https://www.bilibili.com/video/BV1E64y1u71X
【Rust语言教程】基础教程（进阶）
https://www.bilibili.com/video/BV1h94y1D72r
```
